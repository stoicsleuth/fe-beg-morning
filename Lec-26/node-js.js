// a = 10
// console.log(a)

// // Scenario 1
console.log(this)
// // Output -> {}

// // Scenario 2
// // Global Object for executing a function on its own
// function test() {
//     console.log(this)
// }
// test()

// Scenario 3
// Running a function on an object
// let obj = {
//     prop1: "prop",
//     prop2: "prop2",
//     test: function() {
//         console.log(this.prop1)
//     }
// }
// obj.test()

// const nestedObj = {
//     topLevelProp: 10,
//     inner: {
//         innerLevelProp: 20,
//         // innerProp2: this.topLevelProp,
//         testFn: function() {
//             console.log(this.topLevelProp)
//             // console.log(this)
//         }
//     }
// }
// // console.log(nestedObj.topLevelProp)
// nestedObj.inner.testFn()

// Scenario: 4
// What if we nest a function inside an object and run it
// Independently
const obj2 = {
    prop: "prop",
    testObj2: {
        nestedFn: function() {
            console.log(this); 
        }
    },
    testFn: function () {
      console.log(this);
      var nestedFn = function () {
        console.log(this); 
      };
    //   this.nestedFn();
    }
  };

// obj2.testFn()
// obj2.testObj2.nestedFn()