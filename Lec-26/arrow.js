// const obj = {
//     name: "John",
//     regularFunc: function() {
//         console.log("Regular Function", this)
//     }
// }

// // obj.regularFunc()

// this.newProp = "prop12"
// console.log(this) // {}
// const obj2 = {
//     name: "John",
//     inner: {
//         innerMost: {
//             arrowFunction: () => {
//                 console.log("Regular Function", this)
//             } // -> This function was defined in the global scope
//         }
//     }
// }
// obj2.inner.innerMost.arrowFunction()

// // If you have to use a property of the object from inside the
// // fn on the obj, use regular fn
// // If you don't access any property, you can use arrow functions

// const obj2 = {
//     prop: "prop1",
//     innerFn: function() {
//         console.log(this)
//         const arrow = () => {
//             console.log(this)
//         }
//         arrow()
//     }
// }
// obj2.innerFn()

function isolatedFn() { console.log(this) }
isolatedFn()
const obj1 = {
    innerFn: isolatedFn
}
obj1.innerFn() //
const obj2 = {
    nested: {
        innerMost: isolatedFn
    }
}
obj2.nested.innerMost() //
