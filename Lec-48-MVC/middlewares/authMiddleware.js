
const authenticateMiddleWare = (req, res, next) => {
    console.log("Middleware 1 is called")

    if(req.headers.password === "test") {
        // Allow the user to proceed
        next()
    } else {
        res.status(500).send("You are not authenticated!")
    }
}

module.exports = authenticateMiddleWare
