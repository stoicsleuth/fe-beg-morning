const express = require("express")
const router = express.Router()

const { addUserHandler,
    getUsersHandler,
    getUsersByIdHandler,
    deleteUsersHandler } = require("../controllers/usersController")

const authenticateMiddleware = require("../middlewares/authMiddleware")

// Adding routes
router.post("/add-user", authenticateMiddleware, addUserHandler)

// Authenticated routes -> only users with password can access it
router.get('/users', authenticateMiddleware, getUsersHandler)

router.get('/users/:id', authenticateMiddleware, getUsersByIdHandler)

module.exports = router
