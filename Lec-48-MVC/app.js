// Imports
const express = require("express")
const app = express()
app.use(express.json())
const dotenv = require("dotenv")
dotenv.config()

// DB Config
const { connectDB } = require("./config/db")
// It reads any .env file, converts them into JS object, stores them in process.env
const { PORT } = process.env

connectDB()

// Importing any routes, if required
const userRouter = require("./routes/users")


// Registering the routes separately
app.use("/api", userRouter)

// // Un-authenticated -> everyone can access it
// app.get('/hello', (req, res) => {
//     const json = {
//         message: "You have successfully hit the /hello API",
//         timestamp: Date.now()
//     }

//     // Automatically set tyhe content header
//     res.json(json)
// })

app.listen(PORT, () => {
    console.log("Application has started to run!")
})