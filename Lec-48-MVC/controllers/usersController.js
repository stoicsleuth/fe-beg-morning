const {UserModel} = require("../models/users")


const addUserHandler = async (req, res) => {
    try {
        const userDetails = req.body

        const user = await UserModel.create(userDetails)

        res.status(200).json({
            status: "success",
            message: "User created successfully!",
            user
        })
    } catch (error) {
        res.status(500).json({
            status: "failure",
            message: error.message
        })
    }
}


const getUsersHandler = async (req, res) => {
    try {
        const users = await UserModel.find()

        if(users.length === 0) {
            return res.status(404).json({
                status: "failure",
                message: "No users found!"
            })
        }

        res.status(200).json({
            status: "success",
            users
        })
    } catch (error) {
        res.status(500).json({
            status: "failure",
            message: error.message
        })
    }
}

const getUsersByIdHandler = async (req, res) => {
    try {
        const user = await UserModel.findById(req.params.id)

        if(!user) {
            return res.status(404).json({
                status: "failure",
                message: "No user found!"
            })
        }

        res.status(200).json({
            status: "success",
            user
        })
    } catch (error) {
        res.status(500).json({
            status: "failure",
            message: error.message
        })
    }
}

const deleteUsersHandler = async () => {}

module.exports = {
    addUserHandler,
    getUsersHandler,
    getUsersByIdHandler,
    deleteUsersHandler
}