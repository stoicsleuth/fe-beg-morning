const express = require("express")
const movieRouter = express.Router();
const Movie = require("../model/movieModel")
const authMiddleware = require("../middlewares/authMiddleware")


// function sum (request, two) { return a+b } -> Parameters
// sum(1, 10) -> Arguments


// Add a movie
movieRouter.post("/add-movie", authMiddleware, async (req, res) => {
    try {
        const movie = new Movie(req.body)

        await movie.save()

        res.send({
            success: true,
            message: "Movie is added!"
        })

    } catch(e) {
        res.status(500).send({
            success: false,
            message: "Internal Server Error",
            error: e
        })
    }
})


// List all movie
movieRouter.get("/get-all-movies", authMiddleware, async (req, res) => {
    try {
        const movies = await Movie.find()

        res.send({
            success: true,
            message: "Movies fetched!",
            movies
        })

    } catch(e) {
        res.status(500).send({
            success: false,
            message: "Internal Server Error"
        })
    }
})


// Add a movie
movieRouter.put("/update-movie", authMiddleware, async (req, res) => {
    try {
        const movie = await Movie.findOneAndUpdate(req.body.movieId, req.body)

        res.send({
            success: true,
            message: "Movie is updated!",
            movie
        })

    } catch(e) {
        res.status(500).send({
            success: false,
            message: "Internal Server Error"
        })
    }
})

module.exports = movieRouter