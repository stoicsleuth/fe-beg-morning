

1. During hoisting in JavaScript, what gets moved to the top of their containing scope?
   A) Function definitions and variable declarations.
   B) Conditional statements.
   C) Object literals.
   D) Event listeners.

   **Correct Answer:** A) Function definitions and variable declarations.

2. What does the execution context represent in JavaScript?
   A) It determines the scope of a variable.
   B) It organizes the code into functions.
   C) It tracks the order of execution for asynchronous tasks.
   D) It provides the environment within which JavaScript code is executed.

   **Correct Answer:** D) It provides the environment within which JavaScript code is executed.

3. What does the term "temporal dead zone" refer to in JavaScript?
   A) It's a period during which a function is inaccessible.
   B) It's a phase where code execution stops temporarily.
   C)variable is inaccessible until the moment the it's initialized
   D) It's a zone where time-related operations are not allowed.

   **Correct Answer:** C) It's the time between a variable's declaration and its initialization, during which accessing the variable results in a ReferenceError.