import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import AppClass from './components/AppClass.jsx'
import Counter from './components/Counter.jsx'
import TodoList from './components/Todo.jsx'
import TodoFunctional from './components/TodoFunctional.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
  <>    {/* <AppClass text="hello world" label="hello"/> */}
    <TodoFunctional />
    </>
  // </React.StrictMode>,
)
