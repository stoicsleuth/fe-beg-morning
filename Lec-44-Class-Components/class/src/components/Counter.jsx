import React from 'react';

class Counter extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            counter: 0,
            visible: "False"
        }
    }

    render() {
        return <div>
            Counter: {this.state.counter}
            Visible: {this.state.visible}

            <button
            onClick={
                () => this.setState({ counter: this.state.counter + 1 })} >
                Increase
            </button>
            <button onClick={
                () => this.setState({ counter: this.state.counter - 1 })
            }>
            Decrease</button>
        </div>
    }
}

export default Counter