import React from 'react'

// function App({ text }) {

//     return (
//         <>
//             Hello
//         </>
//     )
// }

// <App text="hello world" />

class AppClass extends React.Component {
    render() {
        const { text, label } = this.props

        return (
            <>
                {text}
                {label}
            </>
        )
    }
}
{/* <AppClass text="hello world" /> */}

export default AppClass;