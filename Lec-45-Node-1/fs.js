const fs = require("fs")

// By default, all FS operations are async
// Any kind of I/O operations need to be async if they take a lot of time

// Synchrounous way of reading a file
fs.readFile('file1.txt',  "utf8", (err, data) => {
    if(err) {
        console.error(err)
        console.error("Some problem in reading file")
    }

    console.log(data)
})

console.log("When will I run?")

const res = fs.readFileSync("example.txt", "utf8")

console.log(res)
console.log("When will I run too?")


// Writing a file
fs.writeFile("example.txt", "This is what will be written in the file", "utf8", (err, data) => {
    if(err) {
        console.error(err)
        console.error("Some problem in reading file")
    }

    console.log(data)
})