// Path module -> path module lets us work with relative and absolute path

const path = require("path")

// Joining multiple path
// parent/child/grandchild.txt
// parent\child\grandchild.txt

const fullPath = path.join("parent", "child", "grandchild.txt")

console.log(fullPath)


// Given a relative path, it will return the absolute path
const resolvedPath = path.resolve("parent", "child", "grandchild.txt")

console.log(resolvedPath)

const extension = path.extname(fullPath)

console.log(extension)


// Whenever there are segments like ../. this will resolve them and give us the clean path
const normalizedPath = path.normalize("./path/to/../file.txt")

console.log(normalizedPath)
