// Intereacting with folders using fs module
const fs = require("fs")


// fs.mkdir(), fs.rmdir(), fs.stat()

// fs.mkdir("newdir", null, (err, data) => {
//     if(err) {
//         console.error(err)
//     }

//     console.log(data)
// })

// fs.rmdir("newdir", { recursive: true }, (err, data) => {
//     if(err) {
//         console.error(err)
//     }

//     console.log(data)
// })


fs.stat("example.txt", (err, data) => {
    console.log("data", data)
})