// Functions are first class citizens
// I can pass around functions, and I can return functions from functions

function lonelyFn(a) {
    return a + 5 
}

const res = lonelyFn(10)


function outer(a) {
    return function inner(b) {
        return a + b
    }
}

const innerFn = outer(5)
const innerResult = innerFn(10)