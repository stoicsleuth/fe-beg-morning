// Events in browser
// When X happens, do Y
// We have only attached listeners for events
// For browsers, the events are sent by browser

// Event emitters -> basically classes/objects which can emit custom events

// Event driven programming
const eventEmitter = require('events');
const myEmitter = new eventEmitter();


// How can we create out own Event Emitter

class CustomEventEmitter {
    constructor() {
        this.events = {}
    }

    on(event, handler) {
        if (!this.events[event]) {
            // Initializing the array for the event
            this.events[event] = []
            this.events[event].push(handler)
        }
        else {
            this.events[event].push(handler)
        }
    }

    emit(event, ...args) {
        // Try to figure out if this event has any handler
        // For all the handlers, run them one by one with the args
        if (this.events[event]) {
            this.events[event].forEach((handler) => {
                handler(...args)
            })
        }
    }

    off(event, callback) {
        // Try to find any handler which is same
        // As the callback passed, and then remove it
        if (!this.events[event]) {
        }
        else {
            // const eventToRemove = this.events[event].findIndex((handler) => handler === callback)
            // const newEventArray = this.events[event].slice(eventToRemove, 1)

            const filteredEvents = this.events[event].filter((handler) => handler !== callback)
            this.events[event] = filteredEvents
        }

    }


}

const custEvent = new CustomEventEmitter()

// // Listeners
// myEmitter.on('sendEmail', (...args) => {
//     // Sending an email with the arguments
//     console.log("There is a new event!", args);
// });

// myEmitter.on('sendEmail', (...args) => {
//     // Sending an email with the arguments
//     console.log("There is a new event!", args);
// });



// Down the line, somewhere I need to send the email

// Listeners
custEvent.on('myEvent', (...args) => {
    console.log("There is a new event!", args);
});

custEvent.on('myEvent', (...args) => {
    console.log("another listener for the new event", args);
});

// Emit an event
custEvent.emit('myEvent');
custEvent.emit('myEvent', 1, 2);
custEvent.emit('myEvent', [1, 2, 3]);

// // Removing callbacks
// const secondCb = (...args) => {
//     console.log("another listener for the new event", args);
// };

// myEmitter.on("myEvent", secondCb);
// myEmitter.off('myEvent', secondCb);
// // For this example, because we are removing the handler,
// // There is nothing to handle this event
// myEmitter.emit("myEvent", [1, 2, 3]);