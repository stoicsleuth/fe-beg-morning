const { exec } = require('child_process');

// Exec runs a process which can trigger in terminal/shell
exec('ls -lh', (error, stdout, stderr) => {
    if (error) {
        console.error(`exec error: ${error}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
    console.error(`stderr: ${stderr}`);
});