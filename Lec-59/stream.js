const path = require("path")
const fs = require("fs")


const filePath = path.join(__dirname, 'big.file');
console.log(filePath);
const readableStream = fs.createReadStream(filePath);
const writableStream = fs.createWriteStream('anotherCopyOfBig.file');

//Connects both these streams
readableStream.pipe(writableStream);

readableStream.on('error', (err) => {
    console.log("Error while reading", err);
});
writableStream.on('error', (err) => {
    console.log("Error while writing", err);
});


// WEBRTC