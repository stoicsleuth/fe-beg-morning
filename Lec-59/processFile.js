const { execFile } = require('child_process');
const scriptPath = './script.sh'; // Use 'script.bat' for Windows
// const args = ['arg1', 'arg2'];

// ExecFile runs a process which can execute any file present on the machine
execFile("./script.sh", (error, stdout, stderr) => {
    if (error) {
        console.error(`Execution error: ${error}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
    console.error(`stderr: ${stderr}`);
});