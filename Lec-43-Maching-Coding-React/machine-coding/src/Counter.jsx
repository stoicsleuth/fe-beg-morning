//Build a Counter App/Stopwatch
// Start Button -> Start my timer
// Pause -> Pause my timer
// Resume -> Resume my timer
// Reset -> reset my timer


// Extra challenge -> Display the time in the format hh:mm:ss
// Initially, the value would be 00:00:00


import React, { useState, useRef, useEffect, useCallback } from 'react'

function Counter() {
  const [counter, setCounter] = useState(0)
  const [isRunning, setIsRunning] = useState(false)
  const timerRef = useRef(null)

  useEffect(() => {
    return () => {
        clearInterval(timerRef.current)
    }
  }, [])

  const startTimer = useCallback(() => {
    setIsRunning(true)
    timerRef.current = setInterval(() => {
        setCounter((prev) => prev + 1)
    }, 1000)
  }, [])

  const pauseTimer = useCallback(() => {
    setIsRunning(false)
    clearInterval(timerRef.current)
    // timerRef.current = null
  }, [])

  const stopTimer = useCallback(() => {
    setIsRunning(false)
    clearInterval(timerRef.current)
    setCounter(0)
    // timerRef.current = null
  }, [])

  return (
    <div>
        <div>Time: {counter}</div>
        <button onClick={startTimer} disabled={isRunning}>Start Timer</button>
        <button onClick={pauseTimer} disabled={!isRunning}>Stop Timer</button>
        <button onClick={stopTimer} >Reset Timer</button>
    </div>
  )
}

export default Counter

// Homework: hh:mm:ss representation
// Build a custom timer component so that user can create as many timers as they want in a single page