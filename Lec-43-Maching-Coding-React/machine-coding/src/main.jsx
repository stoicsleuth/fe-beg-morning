import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import AppRef from './AppRef.jsx'
import AppRef2 from './AppRef2.jsx'
import Counter from './Counter.jsx'
import Carousel from './Carousel.jsx'
import ModalApp from './ModalApp.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ModalApp />
  </React.StrictMode>,
)
