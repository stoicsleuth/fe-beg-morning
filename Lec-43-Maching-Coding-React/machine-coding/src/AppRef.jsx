import React, {useRef} from 'react'

// Example 1: Accessing DOM elements

function AppRef() {
  const inputRef = useRef(null)

  console.log({ inputRef })

  const focusInput = () => {
    console.log({ inputRef })
    inputRef.current.focus()
  }

  return (
    <div>
        {/* <Customcomponent /> */}
        <input ref={inputRef} type="text" />
        <button onClick={focusInput}>Focus on the input</button>
    </div>
  )
}

export default AppRef