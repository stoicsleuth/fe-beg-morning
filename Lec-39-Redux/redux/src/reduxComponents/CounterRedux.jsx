import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import counterSlice from '../redux/counterSlice'

const counterAction = counterSlice.actions

const generateRandomBetweenMinandMax = (min, max) => {
    return Math.random() * (max - min) + min 
}

function CounterRedux() {
    const counterSlice = useSelector((store) => store.counter.count)

    const dispatch = useDispatch()

    //   console.log({ counterSlice })

    const handleIncrement = () => {
        dispatch(counterAction.incrementState())
    }

    const handleDecrement = () => {
        dispatch(counterAction.decrementState())
    }

    const handleIncrementByX = () => {
        // Generate random value
        const random = generateRandomBetweenMinandMax(0, 10)

        dispatch(counterAction.incrementByX(random))
    }

    return (
        <>
            <div>CounterRedux {counterSlice}</div>
            <button onClick={handleIncrement}>Increment</button>
            <button onClick={handleDecrement}>Decrement</button>
            <button onClick={handleIncrementByX}>Increment Randomly</button>

        </>
    )
}

export default CounterRedux