import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Counter from './normalComponents/Counter'
import CounterRedux from './reduxComponents/CounterRedux'
import TodoRedux from './reduxComponents/TodoRedux'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      {/* <Counter />
      <CounterRedux /> */}
      <TodoRedux />
    </>
  )
}

export default App
