import { createSlice } from "@reduxjs/toolkit";

const counterSlice = createSlice({
    name: "counterSlice",
    initialState: {
        count: 100,
        randomValue: null
    },
    reducers: {
        // Whatever modifications to this state need to be confined
        // In the reducers here
        incrementState: (state) => {
            state.count = state.count + 1
        },
        decrementState: (state) => {
            if(state.count > 1) {
                state.count = state.count - 1
            }
        },
        createRandomState: (state) => {
            state.randomValue = "123"
        },
        incrementByX: (state, { payload }) => {
            if(payload) {
                state.count = state.count + payload

            }
        }
    }
})

export default counterSlice