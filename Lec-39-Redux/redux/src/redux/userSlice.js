import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
    name: "userSlice",
    initialState: {
        user: {
            name: "Bruce"
        }
    }
})

export default userSlice