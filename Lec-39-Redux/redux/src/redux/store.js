import { configureStore } from "@reduxjs/toolkit";
import counterSlice from "./counterSlice";
import userSlice from "./userSlice";
import todoSlice from "./todoSlice";

// // {
//     todo: {},
//     counter: {}
// //}

const store = configureStore({
    reducer: {
        counter: counterSlice.reducer,
        user: userSlice.reducer,
        todo: todoSlice.reducer
    }
})

export default store