function fetchUserData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            rej("Error")
            res("User data fetched successfully")
        }, 3000)
    })
}

function fetchProductData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            rej("Product data error!")
        }, 2000)
    })
}

function fetchProfileData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            rej("Error")
            res("Profile data fetched successfully")
        }, 4000)
    })
}

Promise.any([fetchUserData(), fetchProductData(), fetchProfileData()]).then((val) => {
    console.log(val)
}).catch((e) => {
    console.log(e)
})