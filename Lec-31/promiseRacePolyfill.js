// Resolve/reject with whichever promise is resolved or rejected first

Promise.myRace = function(promises) {
    return new Promise((res, rej) => {
        promises.forEach((promise) => {
            Promise.resolve(promise)
            .then(res)
            .catch(rej)
        })
    })
}

// Homework -> Promise.allSettled()

// Homework, given a list/array of promises, implement a function which triggers these promises 1-by-1, when the first promise gets resolved, only then trigger the second promise....if there's any rejection in this chain, then immediately reject