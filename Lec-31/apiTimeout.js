function fetchUserData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res("User data resolved!")
        }, 6000)
    })
}

function maxTimeout() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            rej("Timeout")
        }, 5000)
    })
}

Promise.race([fetchUserData(), maxTimeout()])
.then((val) => console.log(val))
.catch((e) => console.log(e))


try {
    const val = await Promise.race([fetchUserData(), maxTimeout()])
    console.log(val)
} catch (error) {
    console.log(error)
}
// axios