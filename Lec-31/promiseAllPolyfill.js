// Store values of resolved promises in array
// If any rejects, then reject Promise.all
Promise.myAll = function(promises) {
    return new Promise((res, rej) => {
        const resultArray = [] // Resultant array of resolved values
        let total = 0; // Count to keep track of how many promises have reoslved till now 

        promises.forEach((promise, index) => {
            Promise.resolve(promise).then((resolvedValue) => {
                resultArray[index] = resolvedValue
                total = total + 1

                if(total === promises.length) {
                    // I am on my last promise resolution
                    res(resultArray)
                }
            }).catch((e) => {
                rej(e)
            })
        })
    })
}

function fetchUserData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res("User data fetched successfully")
        }, 1000)
    })
}

function fetchProductData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            rej("Product data error!")
        }, 2000)
    })
}

function fetchProfileData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res("Profile data fetched successfully")
        }, 3000)
    })
}

// Promise.myAll([fetchUserData(), fetchProductData(), fetchProfileData()]).then((val) => {
//     console.log(val)
// }).catch((e) => {
//     console.log(e)
// })

Promise.myAll([1, 2, 3]).then((val) => {
    console.log(val)
}).catch((e) => {
    console.log(e)
})