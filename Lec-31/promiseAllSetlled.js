function fetchUserData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res("User data fetched successfully")
        }, 1000)
    })
}

function fetchProductData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            rej("Product data error!")
        }, 2000)
    })
}

function fetchProfileData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res("Profile data fetched successfully")
        }, 3000)
    })
}

Promise.allSettled([fetchUserData(), fetchProductData(), fetchProfileData()]).then((val) => {
    console.log(val)
}).catch((e) => {
    console.log(e)
})