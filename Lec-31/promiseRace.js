function fetchUserData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            rej("User data error!")
        }, 1000)
    })
}

function fetchProductData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            rej("Product data error!")
        }, 2000)
    })
}

function fetchProfileData() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res("Profile data fetched successfully")
        }, 3000)
    })
}

Promise.race([fetchUserData(), fetchProfileData(), fetchProductData()]).then((val) => console.log(val)).catch((e) => {
    console.log(e)
})

// API timeout




///// 1(1000) -> 2 (2000)(rej) -3 (3000)