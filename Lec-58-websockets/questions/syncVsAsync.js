// Q: Take a look at the following example and 
// answer if this is a sync process or async process

const express = require('express');
const cors = require('cors');
const app = express();

// Whenever there is any IO operation (Reading a file, writing a file, making a network request)
// They are performed in an async manner
// BUT whenever there is any computation/CPU intensive tasks
// They are performed synchronously


// Prevention measures
// 1: Maybe when we calculate fibonacci series, we put a hard limit
// 2: Run a separate process before starting my server that computes all large fibonacci numbers and stores them in DB
// 3: Use DP to precompute or remeber the values being used for other iterations
// 0, 0 + 1, 1 + 1, 
function calculateFibonacci(number) {
    if (number <= 1) {
        return number;
    }
    return calculateFibonacci(number - 1) + calculateFibonacci(number - 2);
}

app.use(cors());

app.get('/fib', (req, res) => {
    const { number, requestNumber } = req.query;
    console.log("handler fn ran for req", requestNumber);
    if (!number || isNaN(number) || number <= 0) {
        return res.status(400).json({ error: 'Please provide a valid positive number.' });
    }
    const answer = calculateFibonacci(number);
    res.status(200).json({
        status: "success",
        message: answer,
        requestNumber
    });
});

app.listen(3000, () => {
    console.log('Server is running on port 3000');
});