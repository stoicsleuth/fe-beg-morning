// Q: What is the equivalent of window object in Nodejs
console.log(global)

// Q: One more object present in nodes, which contains information
// About the currently running process
console.log(process)

// Q: Can we get a list of all the modules loaded for the current
// process
console.log(process.moduleLoadList)