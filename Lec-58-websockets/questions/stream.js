const http = require('http');
const server = http.createServer();

// Q: Is this the best way of handling a large file?
// Can we do better?

const content = Math.random().toString(36).repeat(10000000); // Approximately 130MB


server.on('request', (req, res) => {
    fs.readFile(content, (err, data) => {
        if (err) throw err;
        res.end(data);
    });
});

server.listen(3000, () => {
    console.log("Server started at 3000");
});


// Streaming