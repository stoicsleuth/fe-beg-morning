const express = require("express");
const http = require("http");
const { Server } = require("socket.io");
const path = require('path');

// Websocket servers use HTTP server, as they are built on top of HTTP,
// That's why we first need to create the HTTP server and pass it to Server()
const app = express();
const server = http.createServer(app);
const io = new Server(server);


// I don't need to create another server to serve my frontend. I can simply use the current express server to serve my html files
app.use(express.static(path.join(__dirname, 'public')));


io.on('connection', (socket) => {
    // Whenver an user connects to our socket server
    // We run this handler
    // socket.id here is a unique identifier for our user
    console.log('a user connected:', socket.id);

    // Send a message from the server to the client
    // The first argument of emit is thge type of event
    // And here the type of event is message
    socket.emit("message", "Welcome to the WebSocket server!");

    // Handle incoming messages from clients
    socket.on("client_message", (msg) => {
        console.log("Message from client:", msg);
        // Broadcast the message to all connected clients
        io.emit("server_message", msg);
    });

    // Handle disconnection
    socket.on('disconnect', () => {
        console.log('user disconnected', socket.id);
    });
});

// io.on('connection', (socket) => {
//     // Join a specific room
//     // The client sends a request to join a specific room
//     socket.on('join_room', (room) => {
//         socket.join(room);
//         console.log(`User ${socket.id} joined room ${room}`);
//         // This will send a message to ALL users of that particular room
//         // That this current user has joined the room
//         socket.to(room).emit('server_message', `A new user has joined room ${room}`);
//     });

//     // Leave a specific room
//     socket.on('leave_room', (room) => {
//         socket.leave(room);
//         console.log(`User ${socket.id} left room ${room}`);
//         socket.to(room).emit('server_message', `User has left room ${room}`);
//     });

//     // Send message to a specific room
//     socket.on('room_message', (data) => {
//         const { room, message } = data;
//         socket.to(room).emit('server_message', message);
//     });
// });

app.get('/', (req, res) => {
    res.send("hello world");
});

server.listen(3000, () => console.log("listening at 3000"));