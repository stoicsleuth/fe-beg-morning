import userSlice from "../userSlice"

const userActions = userSlice.actions

// I need to fetch the data 
// If data is correct, I need to dispatch


const fetchUserMiddleware = (param) => {
    return async (dispatch) => {
        try {
            // Attempt to make the network request
            dispatch(userActions.setLoading())

            const resp = await fetch(`https://jsonplaceholder.typicode.com/users/${param}`);
            
            if(resp.status === 404) {
                return dispatch(userActions.notFound())
            }

            const user = await resp.json();

            console.log({ user })

            dispatch(userActions.setUser(user))
        } catch(e) {
            console.log({ e })

            dispatch(userActions.setError())
        }
    }
}

export default fetchUserMiddleware