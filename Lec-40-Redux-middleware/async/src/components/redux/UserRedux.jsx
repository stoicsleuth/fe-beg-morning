import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from "react-redux";
import fetchUser from '../../redux/middlewares/fetchUserMiddleWare'

function User() {
    // const [user, setUser] = useState(null);
    // const [error, setError] = useState(false);
    // const [loading, setLoading] = useState(true);
    const {user, error, loading, notFound} = useSelector((store) => store.user)
    const dispatch = useDispatch()

    const fetchRandomUser = () => {
        dispatch(fetchUser(Math.floor(Math.random()  * 5 + 1)))
    }

    useEffect(function(){
        dispatch(fetchUser(1))
    },[]);


    const heading = <h2>User Example</h2>;

       //if error 
    if (notFound) {
        return <> {heading}
            <h3>User not found</h3>
        </>
    }

    if (loading) {
        return <> {heading}
            <h3>...Loading</h3>
        </>
    }
    //if error 
    if (error) {
        return <> {heading}
            <h3>Error occurred</h3>
        </>
    }
    return (
        <>
            {heading}
            <h4>Name: {user.name}</h4>
            <h4>Phone: {user.phone}</h4>
            <button onClick={fetchRandomUser}>Fetch Random User</button>
        </>
    )
}

export default User