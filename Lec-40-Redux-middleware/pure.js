// const y = 10
let y = 10

const z = 100

function addToY(x) {
    // This is called a side effect
    y = y + x
    return x + z
}

console.log(addToY(10))
console.log(addToY(10))