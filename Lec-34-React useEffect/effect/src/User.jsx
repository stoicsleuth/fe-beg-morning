import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react'

function User() {
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(null)
    const [user, setUser] = useState({})

    // const [userName, setUserName] = useState(null)

    // useEffect(() => {
    //   setUserName(user.name)
    // }, [user])

    const [ counter, setCounter] = useState(0)
    const [ counter2, setCounter2] = useState(0)

    useEffect(() => {
      // Make network calls or any other async stuff
      const fetchData = async () => {
        try {
          const response = await fetch("https://jsonplaceholder.typicode.com/users/1");
          const data = await response.json()

          setLoading(false)
          setUser(data)
        } catch (error) {
          setError(error)
        }
      }

      // HomeWork: Use this behavior to build debouncing in React
      const timer = setTimeout(() => {
        console.log('Timer ran')
      }, 1000)

      console.log("Running the Effect")
      fetchData()


      // We can't return anything other than a function
      // return "value"

      // This cleanup function will run JUST before this effect is re-run
      return () => {
        console.log("Cleaning up some stuff")
        clearTimeout(timer)
      }
      // Dependency Array
    }, [counter])

    useEffect(() => {
      console.log("Will I run?")
    }, [counter2])

    if (error) {
      return <>Some error ocurred!</>
    }

  return (

    <>
       <button onClick={() => setCounter(counter + 1)}>Re-render component</button>
       <button onClick={() => setCounter2(counter2 + 1)}>Re-render component using counter2</button>

        <div>{loading ? "Loading" : user.name}</div>

    </>
  )
}

export default User