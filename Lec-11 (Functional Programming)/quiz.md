1. What is a key principle of functional programming in JavaScript?

   a) Emphasizing the use of mutable state to manage data.
   
   b) Encouraging side effects and imperative programming techniques.
   
   c) Favoring the use of pure functions and avoiding mutable data.
   
   **Answer: c) Favoring the use of pure functions and avoiding mutable data.**

2. Which of the following best describes higher-order functions in JavaScript?

   a) Functions that are defined within other functions.
   
   b) Functions that take other functions as arguments or return functions as results.
   
   c) Functions that only operate on primitive data types.
   
   **Answer: b) Functions that take other functions as arguments or return functions as results.**

What is the purpose of the map() function in functional programming?

a) To filter elements from an array based on a given condition.

b) To transform each element of an array using a provided function and return a new array.

c) To reduce an array to a single value by performing a specified operation on each element.

Answer: b) To transform each element of an array using a provided function and return a new array.