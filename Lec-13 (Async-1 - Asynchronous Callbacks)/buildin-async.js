const fs = require("fs")

console.log("Start")

fs.readFile('f1.txt', (err, data) => {
    if(err) {
        console.log("Some error ocurred")

        return
    }

    console.log("Data is", data)
    fs.readFile('f2.txt', (err, data) => {
        if(err) {
            console.log("Some error ocurred")
    
            return
        }
        console.log("Data of 2 is", data)

        fs.readFile('f3.txt', (err, data) => {
            if(err) {
                console.log("Some error ocurred")
        
                return
            }
        
            console.log("Data of 3 is", data)
        })
    })
})




console.log("After")