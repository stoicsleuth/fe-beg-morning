// You have an array of objects
// Each of them has a score.
// You need to filter them out by ones which are greater than 80
// And the final result should have one more field called "winner: true"

const array = [
    {
        name: "Raghav",
        score: 110
    },
    {
        name: "Jasbeer",
        score: 60
    },
    {
        name: "Mrinal",
        score: 90
    },
    {
        name: "Tara",
        score: 50
    }
]

// Earlier way of doing this

const finalArray = []

for(let i =0; i < array.length; i++) {
    if(array[i].score > 80) {
        finalArray.push(array[i])
    }
}
// console.log(finalArray)


// HOC 

// Truthy values -> 1, any string, true
// Falsy values -> false, 0, null, undefined
const final2 = array.filter((elem) => elem.score > 80).map((elem) => {
    return {
        name: elem.name,
        score: elem.score,
        winner: true
    }
})
console.log(final2)


// Ways of writing function
function newFunc() {
    console.log("ABC")
}

const newFunc1 = function() {
    console.log("ABC")
}

const newFunc2 = () => {
    return 5
}
const newFunc3 = () => 5