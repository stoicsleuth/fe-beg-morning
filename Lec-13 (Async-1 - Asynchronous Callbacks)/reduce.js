// Problem statement
// You have a bunch of numbers in an array
// Your job is to sum them

const array = [1, 3, 5, 6, 8, 3, 10]

// Approach 1
let finalSum = 0

for(let i = 0; i < array.length; i ++) {
    finalSum = finalSum + array[i]
}

// console.log("Final sum is", finalSum)

// Approach 2

const finalValueRecuce = array.reduce((acc, elem) => {
    acc = acc + elem

    return acc
})

const finalValueRecuce2 = array.reduce((acc, elem) =>  acc + elem)

console.log("Final sum is", finalValueRecuce2)
