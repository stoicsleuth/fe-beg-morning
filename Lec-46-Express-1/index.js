const express = require("express")

const app = express()

// Middleware 
// This is an in-build middleware
app.use(express.json())

const PORT = 3000


let USERS_ARRAY = [{
    userId: 1,
    name: "Bruce",
    address: "Gotham"
}, {
    userId: 2,
    name: "Riddler",
    address: "Gotham"
}]

const json = {
    message: "You have successfully hit the / API",
    timestamp: Date.now(),
    isRoot: true
}

// Registering the routes separately

const authenticateMiddleWare = (req, res, next) => {
    console.log("Middleware 1 is called")

    if(req.headers.password === "test") {
        // Allow the user to proceed
        next()
    } else {
        res.status(500).send("You are not authenticated!")
    }
}

const testMiddleWare = (req, res, next) => {
    //Dummy middleware
    console.log("Middleware 2 is called")
    next()
}

// Authenticated routes -> only users with password can access it
app.get('/users', [testMiddleWare, authenticateMiddleWare], (req, res) => {
    // Automatically set tyhe content header
    res.json(USERS_ARRAY)
})

app.get('/users/:userId', authenticateMiddleWare, (req, res) => {
    console.log(req.params)
    const user = USERS_ARRAY.find((user) => user.userId === parseInt(req.params.userId, 10))
    // Automatically set tyhe content header
    res.json(user)
})


app.delete("/delete-user", (req, res) => {
    const formData = req.body

    USERS_ARRAY = USERS_ARRAY.filter((user) => {
        return user.userId !== formData.userId
    })

    res.json({
        success: true
    })
})

app.post("/add-user", (req, res) => {
    // How do I get the data that was sent with the request
    const formData = req.body

    console.log(req.body)

    USERS_ARRAY.push({
        userId: formData.userId,
        name: formData.name,
        address: formData.address
    })

    res.json({
        success: true
    })
})

// Un-authenticated -> everyone can access it
app.get('/hello', (req, res) => {
    const json = {
        message: "You have successfully hit the /hello API",
        timestamp: Date.now()
    }

    // Automatically set tyhe content header
    res.json(json)
})

// In build middleware that runs for any route not specified above
// // Catch-all route to indicate 404 error
// app.use((req, res) => {
//     res.status(404).send("This route was not found")
// })

app.listen(PORT, () => {
    console.log("Application has started to run!")
})