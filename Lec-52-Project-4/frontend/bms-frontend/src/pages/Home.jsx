import React from 'react'
import { message } from 'antd'

function Home() {
  const [messageApi, contextHolder] = message.useMessage();

  return (
    <div>
      {contextHolder}
      Home</div>
  )
}

export default Home