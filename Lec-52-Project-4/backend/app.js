const express = require("express")
const app = express()
const dotenv = require("dotenv")
var cors = require('cors')
dotenv.config()
const userRouter = require("./routes/userRoute")
const movieRouter = require("./routes/movieRoute")


// Add in middleware to handle request body as JSON
app.use(express.json())
app.use(cors())


// Registering my root level routes
app.use("/api/user", userRouter)
app.use("/api/movies", movieRouter)


const {connectDB } = require("./config/db")
connectDB()

app.listen(process.env.PORT, () => {
    console.log("Backend application has started!")
})