// function printNameAndAge() {
//     console.log(`Name is ${this.name} and age is ${this.age}`)
// }

const person1 = {
    name: "Bruce Wayne",
    age: 34, 
    printNameAndAge: function(location) {
        console.log(`Name is ${this.name} and age is ${this.age}, residing at ${location}`)
    },
    sum: function (num1, num2, num3, num4, num5) {
        return num1 + num2 + num3 + num4 + num5
    }
}

person1.sum(1, 2, 3, 4, 5)

const person2 = {
    name: "Selina Kyle",
    age: 29,
    // printNameAndAge: printNameAndAge
}

const args = [1, 2, 3, 4, 5]

person1.printNameAndAge.call(person2, args[0], args[1], args[2], args[3], args[4])




