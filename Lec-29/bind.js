const person1= {
    name: "Bruce Wayne",
    age: 34, 
    printNameAndAge: function(location) {
        console.log(`Name is ${this.name} and age is ${this.age}, residing at ${location}`)
    },
}


// Call & Apply both executes the function and returns whatever the function is returnong
// Bind will not call the fn, it simply returns a (new) fn that "remembers" the context
const printNameAndAgeBatman = person1.printNameAndAge.bind(person1, "Gotham")
const printNameAndAgeCatwoman = person1.printNameAndAge.bind({
    name: "Selina Kyle",
    age: 29
})


printNameAndAgeBatman()
// person1.printNameAndAge()
// person1.printNameAndAge.call(person2)