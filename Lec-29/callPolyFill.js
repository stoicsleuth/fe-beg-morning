// Array.map()

// A browser doesn't support call, apply and bind

Function.prototype.myCall = function(context, ...args) {
    // Check if I am calling myCall on a function
    if(typeof this !== 'function') {
        throw new Error("Must be called on a function")
    }
    // Set this function on the new object by creating a temporary key
    // Then simply call the fn on the context object with the variables
    context.tempFunc = this
    context.tempFunc(...args)
}

// If we want to add a default value to our function parameters
// that means the value we want to use when we are passing undefined
// then we can simply do arg = value
Function.prototype.myApply = function(context, argsArray = []) {
    // Check if I am calling myCall on a function
    if(typeof this !== 'function') {
        throw new Error("Must be called on a function")
    }
    // Set this function on the new object by creating a temporary key
    // Then simply call the fn on the context object with the variables
    context.tempFunc = this
    context.tempFunc(...(argsArray || []))
}

const person1 = {
    name: "Bruce Wayne",
    age: 34, 
    printNameAndAge: function(location) {
        console.log(`Name is ${this.name} and age is ${this.age}, residing at ${location}`)
    },
}

const person2 = {
    name: "Bruce Wayne 2",
    age: 34, 
}
person1.printNameAndAge.myCall(person2)
person1.printNameAndAge.myApply(person2, [1, 2, 3])


// HOMEWORK: Polyfill of Bind
