let promise1 = new Promise(function (resolve, reject) {
    // You will have your async task
    // Simulating a coin toss
    setTimeout(() => {
        const isHeads = Math.random() > 0.5

        if(isHeads) {
            resolve("Heads")
        } else {
            reject("Tails - this is a failure case")
        }
    }, 1000)
})

console.log(promise1, "This is the first state")

promise1.then((result) => {
    console.log(promise1, "This is the final state")
    console.log(result)
}).catch((error) => {
    console.log(promise1, "This is the final state")
    console.log(error)
})


// Just to show, 
// We don't actually use promises for sync code
let promise2 = new Promise(function (resolve, reject) {
    // You will have your async task
    // Simulating a coin toss
    const isHeads = Math.random() > 0.5

    if(isHeads) {
        resolve("Heads")
    } else {
        reject("Tails - this is a failure case")
    }
})

console.log(promise2, "This is the initial status of promise2")
