const fs = require("fs")

// fs.readFile("f91.txt", (err, data) => {
//     if(err) {
//         console.log("Oops, something went wrong!")
//     } else {
//         console.log("Content of the file", data)
//     }
// })

const readFilePromise1 = fs.promises.readFile("f1.txt", "utf-8")

// readFilePromise1.then((data) => {
//     console.log("File reading is done", data)

//     return fs.promises.readFile("f2.txt", "utf-8")
// }).then((data) => {
//     console.log("File 2 reading is done", data)

//     return fs.promises.readFile("f3.txt", "utf-8")
// })
// .then((data) => {
//     console.log("File 3 reading is done", data)

//     console.log("All done")
// })
// .catch((error) => {
//     console.log(error)
// })


readFilePromise1
.then((data) => fs.promises.readFile("f2.txt", "utf-8"))
.then((data) => fs.promises.readFile("f3.txt", "utf-8"))
.then((data) => {
    console.log("All done")
})
.catch((error) => {
    console.log(error)
})

// readFilePromise2.then((data) => {
//     console.log("File 2 reading is done", data)
// }).catch((error) => {
//     console.log(error)
// })

// readFilePromise3.then((data) => {
//     console.log("File 3 reading is done", data)
// }).catch((error) => {
//     console.log(error)
// })