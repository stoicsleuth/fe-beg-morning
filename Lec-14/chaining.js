const cleanRoom = function () {
    return new Promise((res, rej) => {
        setTimeout(() => {
            // console.log("Cleaning room completed")
            res("Cleaned the room")
        }, 2000)
    })
}

const removeGarbage = function () {
    return new Promise((res, rej) => {
        setTimeout(() => {
            // console.log("Removing garbage completed")
            res("Then Removed Garbage")
        }, 1000)
    })
}

const getIceCream = function (message) {
    return new Promise((res, rej) => {
        res(message, "Won ice cream")
    })
}

cleanRoom()
.then((result) => {
    return removeGarbage()
}).then((result) => {
    return getIceCream(result)
}).then(() => {
    console.log("Everything is done!")
}).catch((error) => {
    console.log(error, "Error")
}).finally(() => {
    console.log("All done")
})

// Problem statement
// Create a promise, inside the promise
// Do a random coin toss, if it is heads
// Then create one more promise
// Where you always "succeed" after 3 second
// Then finally console log the result of the coin toss
// Additionally, if the coin toss fails, handle the failure in a catch block