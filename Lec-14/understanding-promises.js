function logA() { console.log('A') }
function logB() { console.log('B') }
function logC() { console.log('C') }
function logD() { console.log('D') }
function logX() { console.log('X') }
function logY() { console.log('Y') }


// Homework
logA();
setTimeout(() => {
    logB()
    setTimeout(() => {
        logX()
    }, 0)
    Promise.resolve().then(logY);
}, 0);
Promise.resolve().then(logC);

logD();