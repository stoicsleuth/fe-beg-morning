const arr = [1, [2, 3, [4, 5, 6, 7, 8]]] // n-D Array/ Infinitely nested Array // [1, 2, 3, 4, 5, 6, 7, 8] // 1 D array

// [1, 2, 3, [4, 5, 6, 7, 8]]


function flattenArray(array) {
    return array.reduce((accumulator, currentElem) => {
        // If the current ELement is an Array, do something
        if (Array.isArray(currentElem)) {
            // innerFlattenedArray is an 1D array
            const innerFlattenedArray = flattenArray(currentElem)
            accumulator.push(...innerFlattenedArray)
            // [1, 2, 3] => acc.push(...[1, 2, 3]) => acc.push(1, 2, 3)
        } else {
        // If the current elemnt is not an array, simply push it to my acc
            accumulator.push(currentElem)
        }

        return accumulator
    }, [])
}
// Add a second argument to myFlatten, which will stop the recursion at that level. If don't pass anything, it will only run till the first level

console.log(flattenArray(arr))