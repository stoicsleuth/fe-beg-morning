// I have a transaction Array, I want to create a new Array with just the transaction IDs
const transactions = [{
    id: "credit001",
    amount: 100
}, {
    id: "debit001",
    amount: 80
}, {
    id: "credit002",
    amount: 120
}] // ["credit001", "debit001", "credit002"]

// Check if the function exists, if not, write custom implementation

if (!Array.prototype.myMap) {
    Array.prototype.myMap = function(callback) {
        //Step1: If callback is not a function, then throw
        if(typeof callback !== "function") {
            throw new TypeError("Please pass a callback function!")
        }

        // Step2: Create a new Array that will be returned
        const result = []

        // Step3:// Iterate over the array and run the callback, add the value in the result

        for(let i = 0; i <this.length; i++) {
            if(i in this) {
                const mappedValue = callback(this[i], i, this)

                // Step4: Push the new mappedValue into the final Array
                result[i] = mappedValue
            }
        }

        return result
    }
}

const transactionIds = transactions.map((transactionCurrent) => transactionCurrent.id)

const transactionIdsMyMap = transactions.myMap((transactionCurrent) => transactionCurrent.amount)

const arr1 = [1, 2]
arr1[10] = 100
const newwArr1 = arr1.myMap((elem) => elem + 1)

console.log(arr1, newwArr1)