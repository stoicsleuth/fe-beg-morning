const transactions = [{
    id: "credit001",
    amount: 100,
    type: "credit"
}, {
    id: "debit001",
    amount: 80,
    type: "debit"
}, {
    id: "credit002",
    amount: 120,
    type: "credit"
}] 
//Output: Array of tyransactions where the type is "credit" [{
//     id: "credit001",
//     amount: 100,
//     type: "credit"
// }, {
//     id: "credit002",
//     amount: 120,
//     type: "credit"
// }]

if (!Array.prototype.myFilter) {
    Array.prototype.myFilter = function(callback) {
            //Step1: If callback is not a function, then throw
            if(typeof callback !== "function") {
                throw new TypeError("Please pass a callback function!")
            }
    
            // Step2: Create a new Array that will be returned
            const result = []
    
            // Step3:// Iterate over the array and run the callback, add the value in the result
    
            for(let i = 0; i <this.length; i++) {
                if(i in this) {
                    const conndition = callback(this[i], i, this)
    
                    // Step4: Push the element in this index if condition is true 
                    if(conndition) {
                        result.push(this[i])
                    }
                }
            }
    
            return result
    }
}

const creditTransactions = transactions.filter((currentElem) => currentElem.type == "credit")

const creditTransactionsMyFilter = transactions.myFilter((currentElem) => currentElem.type == "credit")

console.log(creditTransactions, creditTransactionsMyFilter)