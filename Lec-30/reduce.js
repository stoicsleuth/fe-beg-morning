const arr = [1, 2, 3, 4, 5] // 1 + 2 + 3 + 4 +5 => 15

if (!Array.prototype.myReduce) {
    Array.prototype.myReduce = function(callback, initialValue) {
        // Step1: Check for invalid callback value
        if(typeof callback !== "function") {
            return new TypeError("Please pass a callback function")
        }

        // If initialValue is empty, make sure there are values in the Array
        if(!initialValue && this.length < 1) {
            return new TypeError("Array must have values if initialValue is empty")
        }

        // Step3: Initialize the accumulator, and keep track of the initial loop value

        let accumulator = initialValue ? initialValue : this[0]
        let startIndex = initialValue ? 0 : 1

        for(let i = startIndex; i < this.length; i++) {
            if(i in this) {
                accumulator = callback(accumulator, this[i], i, this)
            }
        }

        return accumulator
    }
}

// If there is no initial Value passed to reduce, then, the initialValue becomes arr[0] and the loop starts from the arr[1] element

const reducedValue = arr.reduce((acc, item) => acc + item)
const myReducedValue = arr.myReduce((acc, item) => acc + item)

console.log(reducedValue, myReducedValue)

// Other Higher Order funtions to write polyfill for
// .find, .findIndex