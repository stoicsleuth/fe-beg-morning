// Numbers
// let a = 20
// let b = 20.01
// let c = -20

// const f = 2/0
// //Infinity is special value of a number
// console.log(f)


// //NaN -> this is also a type of number
// const f1 = "Hello"/0
// console.log(f1)

// String
// let a = "Hello World"
// let b = 'Single quoted hello world'

// // String literal
// let c = `Is there any difference`

// const x = 999999
// const y = 10

// // Earlier methods
// const result = x + " number of people watching"
// console.log(result)

// const resultNew = `${x} number of people watching, ${y} is a random number`
// console.log(resultNew)

// const resultNewer = `The value of 2+3 is ${2+3}`
// console.log(resultNewer)

// Boolean

// let isEven = true
// let isOdd = false


// Undefined and Null
let a;
console.log(a)

let b = null;
console.log(b)