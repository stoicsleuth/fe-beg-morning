const b = 10
const c = "Hello"

console.log(b)
console.log(c)
function serveBeverage(drink) {
    console.log(`${drink} is served!`)
}

serveBeverage('Coffee')
serveBeverage('Tea')

console.log(serveBeverage)