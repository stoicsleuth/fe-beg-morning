
## Event delegation

1. Event delegation is a powerful pattern in JavaScript, particularly useful when working with dynamically added elements or when optimizing performance for applications with many event listeners.

2. It leverages the fact that most events in JavaScript bubble up through the DOM, meaning that an event fired on a child node will propagate up to its parent nodes.

3. By taking advantage of this behavior, you can set a single event listener on a parent element to manage all events that bubble up from its children, rather than setting an event listener on each child individually.

Imagine you're browsing a website like Amazon, and on the product listing page, there are multiple product cards, each containing an "Add to Cart" button. Here's how event delegation comes into play:

1. **Event Delegation:**
   Event delegation involves attaching a single event listener to a common ancestor element (in this case, the container holding the product cards). Instead of placing event listeners on each "Add to Cart" button individually, you attach one listener to the container.

2. **Event Bubbling:**
   Event bubbling refers to the natural propagation of an event through the DOM hierarchy. When an event occurs on a deeply nested element, it first triggers the event handler on that element and then "bubbles up" through its ancestors as you have already seen in the previous class

Now, let's see how these concepts work together:

1. When a user clicks the "Add to Cart" button on a product card, the event starts at the button (target) and then bubbles up through its parent elements.

2. Since you've attached a single event listener to the container holding the product cards, the event bubbles up to the container.

3. The event listener captures the event at the container level and checks whether the clicked element (the button) matches certain criteria (e.g., having the class `add-to-cart`).

4. If the criteria are met, the listener knows that an "Add to Cart" action is intended and can extract information about the specific product from the event's context

Let's go through an example with code to demonstrate event delegation and event bubbling using different categories of products (headphones, laptops, mobiles) on a web page:

**HTML Structure:**

```html
<div id="categories">
  <div class="category" id="headphones">
    <h2>Headphones</h2>
    <div class="product">Product A</div>
    <div class="product">Product B</div>
  </div>
  <div class="category" id="laptops">
    <h2>Laptops</h2>
    <div class="product">Product X</div>
    <div class="product">Product Y</div>
  </div>
  <div class="category" id="mobiles">
    <h2>Mobiles</h2>
    <div class="product">Product P</div>
    <div class="product">Product Q</div>
  </div>
</div>
```

**JavaScript:**

```javascript
const categoriesContainer = document.getElementById("categories");

categoriesContainer.addEventListener("click", (event) => {
  const clickedElement = event.target;

  // Check if the clicked element is a product
  if (clickedElement.classList.contains("product")) {
    const category = clickedElement
      .closest(".category")
      .querySelector("h2").textContent;
    const product = clickedElement.textContent;

    console.log(`Clicked on ${product} in the ${category} category.`);
    // Handle the click action for the product here
  }
});
```

In this example:

1. The `categoriesContainer` element is the common ancestor for all categories and products.

2. The event listener is attached to the `categoriesContainer` to capture clicks on any of its child elements.

3. When a product is clicked, the event bubbles up through the category section, reaching the `categoriesContainer`.

4. The listener checks if the clicked element has the class `product`. If it does, it extracts the category and product information and performs the necessary action.

5. This code efficiently handles clicks on products within any category, demonstrating the combined usage of event delegation and event bubbling.

With this setup, regardless of the number of categories or products, you only need one event listener to handle all clicks, making your code more maintainable and efficient.