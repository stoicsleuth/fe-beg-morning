// Selection

// When I click on Auto
// 1. Auto button should become active (green)
// 2. All other buttons should be disabled
// 3. Counter should increase by 1 every second.
// 4. When I click on auto again, then everything should reset

const decrementButton = document.getElementById('decrement');
const incrementButton = document.getElementById('increment');
const resetButton = document.getElementById('reset');
const autoButton = document.getElementById('auto');
const countDisplay = document.getElementById('count');

let count = 0

// Add in the toggle for auto
let isAutoModeEnabled = false

// Store the timer
let timer = null

autoButton.addEventListener('click', () => {
    if(!isAutoModeEnabled) {
        isAutoModeEnabled = true
        incrementButton.classList.add("disabled")
        decrementButton.classList.add("disabled")
        resetButton.classList.add("disabled")
        autoButton.classList.add('auto')

        timer = setInterval(() => {
            count = count + 1
            countDisplay.innerText = count
        }, 1000)
    } else {
        if(timer) {
            clearInterval(timer)
        }

        isAutoModeEnabled = false
        incrementButton.classList.remove("disabled")
        decrementButton.classList.remove("disabled")
        resetButton.classList.remove("disabled")
        autoButton.classList.remove('auto')
        count = 0
        countDisplay.innerText = count
    }
})

decrementButton.addEventListener('click', ()=> {
    if(count > 0) {
        count = count - 1;
        countDisplay.innerText = count
    }
})

incrementButton.addEventListener('click', ()=> {
    count = count + 1;
    countDisplay.innerText = count
})

resetButton.addEventListener('click', ()=> {
    count = 0;
    countDisplay.innerText = count
})