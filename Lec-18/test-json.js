// Happening at location A

const toys = {
    toy1: {name: "Teddy Bear", color: "Brown"},
    toy2: {name: "Race Car", color: "Red"},
    toy3: {name: "Doll", color: "Pink"}
};

const toysToString = JSON.stringify(toys)
console.log(toysToString)

// toysToString Sent from location a to location B

const unpackedToys = JSON.parse(toysToString)
console.log(unpackedToys, unpackedToys.toy1.name)

