
* What is JSON? and How to work with JSON data
* Create a weather app using an API.
* We will be using the "weather API" for getting the data of real-time weather and use that data in our app. 

### JSON

JSON, which stands for JavaScript Object Notation, is a lightweight format for storing and transporting data. It is easy for humans to read and write, and easy for machines to parse and generate. JSON is based on the JavaScript programming language, but it is language-independent, meaning it can be used with many programming languages, including Python, Ruby, PHP, and many others. Let's break this down into simpler concepts:

### 1. What is JSON?

Imagine you're writing down your shopping list in a way that you want both your friends and a computer to understand. You would choose a simple, structured format, right? JSON is exactly that but for data. It uses a set of rules to represent data as text, making it easy to send this data across the internet or save it in files.

### 2. Structure of JSON

JSON's format is reminiscent of JavaScript object literal syntax, but with some restrictions. It mainly consists of two structures:

- **Objects**: These are collections of key/value pairs. In JSON, an object begins with `{` (left brace) and ends with `}` (right brace). Each key is followed by a colon (`:`) and then the value assigned to that key. Key-value pairs are separated by commas. For example:

  ```json
  {
    "name": "John Doe",
    "age": 30,
    "isEmployed": true
  }
  ```

- **Arrays**: These are ordered lists of values. In JSON, an array begins with `[` (left bracket) and ends with `]` (right bracket). Values are separated by commas. For example:

  ```json
  ["apple", "banana", "cherry"]
  ```

Values in JSON can be strings (text wrapped in double quotes `" "`), numbers, objects, arrays, `true`, `false`, or `null`.

### 3. How JSON Works with JavaScript

Now , the major application of this JSON format is to pass data from one System to another , As Most of the internet's data transmission protocols, like HTTP, are text-based. They are designed to transmit data as text rather than in proprietary formats (objects or arrays). JSON, being a text-based format, fits seamlessly into this architecture.

So Now as JSON is text(string) based so to transfer data from one system another we need to convert data that we need to transfer into JSON string

Imagine you have a box of toys. In the world of web development, your toys are your data, like names, ages, or places. Now, you want to send this box to a friend who lives far away. But there's a catch: you can't send the box as it is; you need to pack it in a special way so that it can travel through the internet. This is where **JSON** comes into play.

**JSON** is a way to pack your data (toys) so that they can travel easily over the internet. It turns your data into a string (a sequence of letters, numbers, and symbols) that can be sent and then unpacked by your friend (or another computer).

A simple example to illustrate both parsing and stringifying with toys as our data. First, we'll "pack" our toy data into a JSON string using `JSON.stringify()`, and then we'll "unpack" it using `JSON.parse()`.

```javascript
// Define our toys as a JavaScript object
var toys = {
  toy1: {name: "Teddy Bear", color: "Brown"},
  toy2: {name: "Race Car", color: "Red"},
  toy3: {name: "Doll", color: "Pink"}
};


// Pack our toys into a box (convert our toys object into a JSON string)
var toysJSONString = JSON.stringify(toys);
console.log("Packed Toys:", toysJSONString);


// Here We have converted the javascript object into JSON string now this is understood by the protocolos

// Now, let's say this JSON string is sent over the internet.
// On the other side, we receive this packed box of toys as a JSON string.


// Now as we are recieving a JSON string , so while writing javascript code we will not be able to apply object properties in a JSON string , so now to convert the string again to object we will parse it


// Unpack our toys from the box (parse the JSON string back into a JavaScript object)
var unpackedToys = JSON.parse(toysJSONString);
console.log("Unpacked Toys:", unpackedToys);

// Now, 'unpackedToys' is a JavaScript object with our toys,
// and we can access and work with them like before.
console.log("Let's play with the:", unpackedToys.toy1.name); // Accessing the Teddy Bear
```

In this example:
- We start with our toys organized in a JavaScript object.
- We "pack" these toys using `JSON.stringify()`, turning them into a string that can be easily sent over the internet.
- We simulate receiving this packed string on the other side and then "unpack" it using `JSON.parse()`, turning it back into a JavaScript object that we can work with.
- Finally, we access one of the toys from our unpacked object to show that it's back to its original, usable form.


### 4. Why Use JSON?

JSON is popular for several reasons:

- It is text, and can be read and written by humans.
- It is easy for machines to parse and generate.
- It is fully compatible with JavaScript and many other languages, making it a good choice for data interchange on the web.

In summary, JSON is a universally accepted format for structuring data, making it an essential part of modern web development. Its simplicity, ease of use, and compatibility with many programming languages have made it a preferred choice for data interchange on the internet.



### API and the Use of JSON Data

After understanding the basics of JSON and its importance in web development, it's crucial to explore how JSON data is utilized in APIs (Application Programming Interfaces). APIs play a pivotal role in enabling different software applications to communicate with each other. They act as intermediaries that allow applications to exchange data and functionality easily and securely. JSON, with its simplicity and efficiency, is widely used as the format for exchanging data in API communications. Let's delve deeper into APIs and the role of JSON data in this context.

#### What is an API?

An API is a set of rules and protocols that allows one software application to access the data or functionality of another application, server, or service. APIs are like menus in a restaurant; the menu provides a list of dishes you can order, along with a description of each dish. When you specify which dish you want, the kitchen (the system) prepares the dish and serves it. In the digital world, APIs work similarly: they allow developers to request specific data or actions, and the system responds accordingly.


#### The Role of JSON in APIs

JSON is the medium through which data is exchanged between clients and servers in many APIs. It serves as the "language" that both the requesting and the responding parties understand. Here's how JSON is used in the context of APIs:

1. **Requesting Data**: When an application requests data from a server, it sends an API call, which is essentially a request made over the internet. This request often includes data formatted as a JSON string. For example, an application might request the details of a user by sending a JSON string that specifies the user ID.

2. **Responding to Requests**: Upon receiving the request, the server processes it and sends back a response. This response is typically also in JSON format, containing the data requested or confirmation of the action taken. For instance, a server might respond with a JSON object that includes the user's details.

3. **Ease of Integration**: The reason JSON is so popular in API development is its ease of integration with most programming languages. Libraries in Python, JavaScript, Ruby, and many others can easily parse JSON strings into native data structures of the respective language (like dictionaries in Python or objects in JavaScript) and vice versa.

4. **API Endpoints**: APIs are structured around endpoints, each serving a specific function. An endpoint for retrieving user data might return a JSON object with user attributes, while another endpoint for posting user data might accept JSON-formatted user details.


Overall an API is just an interface which will allow two systems to communicate with each other with each other and the format it uses is JSON , there are other formats as well and ways as well lie SOAP architecture is there which uses XML  simlarly what we are are learning falls under REST where we use JSON as mode of communication.


Weather App ->

- It will have a search bar where you can enter any cities name an their will be a button to get it's weather Info.

- As soon as you click on the button you should be served with that cities weather Information in a decent looking card

These properties should be shown compulsarily- 
  - City Name
  - Temparature
  - Condition (hot , humid , cold , Rainy etc)
  - current date and time in the city

  Rest you can add any other property that you want to show!

How we are going to get the data? An API

Now What can be the steps to get the data from an API?
Suppose there is a weather API which has data about the weather conditions of diffrent places , how should we communicate with that and get exact data that we are looking for?

**Steps to get the data from an API -**

1. The application sends a request to the weather API's endpoint, possibly including the location's coordinates in a JSON-formatted request body.
2. The weather API processes this request and responds with a JSON object containing weather details like temperature, humidity, and weather conditions.
3. The application then parses this JSON response and uses the data to display the weather information to the user.





**To start creating this app we need access to API from where we are going to get the data**

For our app we will be using the API from here -
> https://www.weatherapi.com/


* Find the API key and paste it under the **API explorer** tab. 
* Select the protocol as **HTTPS**.
* Click on show response. You can see the JSON format in the response section.
 

* After logging in, there will be an API key generated which will be unique for everyone. Copy that key first.
* Under **API Response Fields**, you can check all the fields that the API is providing. There are many options available. 
* Under **API Explorer**, paste the key. You will see the API Call as follows.


Here the "q" stands for **query** and here we are querying for London. The call is basically our URL and inside this we also have our API key and the search query. 

* Apart from the current data, you can also get the Forecast, Future data as provided by the API. 



