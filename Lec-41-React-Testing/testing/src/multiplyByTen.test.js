


// Test suite (A collection of tests)

import { expect } from "vitest"
import multiplyByTen from "./multiplyByTen"

describe('Testing multiplying by ten function', () => {
    // Test case (Indivual test that verifies one functionality)
    it("Returns the correct result", () => {
        const result = multiplyByTen(10)

        expect(result).toBe(100)
    })

    it("Handles the case with no input", () => {
        const result = multiplyByTen()

        expect(result).toBeNull()

    })
})