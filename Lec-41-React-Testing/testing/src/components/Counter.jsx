import React, { useState } from 'react'

function Counter() {
  const [count, setCount] = useState(0)

  return (
    <div>
        Counter {count}
        <button data-test-id="increase" onClick={() => setCount(count+1)}>Increase</button>
        <button disabled={count < 1} onClick={() => setCount(count-1)}>Decrease</button>
    </div>

  )
}

export default Counter