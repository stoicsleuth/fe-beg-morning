


// Test suite (A collection of tests)

import { expect } from "vitest"
import { render, screen } from "@testing-library/react"
import Counter from "../Counter"
import userEvent from "@testing-library/user-event"

describe('Testing our counter component', () => {
    // Test case (Indivual test that verifies one functionality)
    it("Component is rendered correctly", () => {
        // Step 1: Render the component
        render(<Counter />)

        // Step2: Select the UI elements
        const counterText = screen.getByText("Counter 0")
        const incrementText = screen.getByText("Increase")
        const decrementText = screen.getByText("Decrease")

        // Step 3: Assert
        expect(counterText).not.toBeNull()
        expect(incrementText).not.toBeNull()
        expect(decrementText).not.toBeNull()
    })

    it("When clicked on increase, the count should increase by 1", async () => {
        // Step 1: Render the component
        render(<Counter />)

        // 2: Select the UI
        const increaseButton = screen.getByText("Increase")

        // 2: Simulate Click
        await userEvent.click(increaseButton)

        // 4: Assert the new changes
        const counterText = screen.getByText("Counter 1")
        expect(counterText).toBeInTheDocument()
    })

    // Homework: Write the same test case, but for decreasing
    // If counter value is 0, my button should be disabled
})