function multiplyByTen(x) {
    if(!x) {
        return null
    }

    return x * 10
}

export default multiplyByTen