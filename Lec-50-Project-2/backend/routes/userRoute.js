const express = require("express")
const bcrypt = require("bcryptjs")
const UserModel = require("../model/userModel")
const userRouter = express.Router()

var validator = require("email-validator");

const SALT_ROUNDS = 12

userRouter.post("/register", async (req, res) => {
    try {
        const isEmailValid = validator.validate(req.body.email)

        if (!isEmailValid) {
            return res.status(500).send({
                success: false,
                message: "Please enter a valid email"
            })   
        }

        // Find if there is an user with the email
        const isUserPresent = await UserModel.findOne({
            email: req.body.email
        })
        if (isUserPresent) {
            return res.status(500).send({
                success: false,
                message: "Email is already taken!"
            })   
        }

        // Create a new User object locally
        const user = new UserModel(req.body)

        //Generating salt and hashing our password

        const salt = await bcrypt.genSalt(SALT_ROUNDS)
        const hashedPassword = await bcrypt.hash(req.body.password, salt)

        user.password = hashedPassword

        // Then save it to Database
        await user.save()

        return res.send({
            success: true,
            message: "registration is successful",
            user
        })
    } catch(e) {
        console.log(e)
        res.status(500).send({
            success: false,
            message: "Internal Server Error"
        })
    }

})


userRouter.post("/login", async function (req, res){
    try {
        // Create a new User object locally
        const user = await UserModel.findOne({
            email: req.body.email
        })

        if(!user) {
            return res.status(404).json({
                success: false,
                message: "No user found"
            })
        }

        // Checking if password is valid or not
        // if(req.body.password !== user.password) {
        //     return res.status(404).send({
        //         success: false,
        //         message: "No user/pass combo found"
        //     })
        // }

        const isPasswordValid = await bcrypt.compare(req.body.password, user.password)
        if(!isPasswordValid) {
            return res.status(404).send({
                success: false,
                message: "No user/pass combo found"
            })
        }

        res.send({
            success: true,
            message: "Successfully logged in!"
        })
    } catch(e) {
        console.log(e)
        res.status(500).send({
            success: false,
            message: "Internal Server Error"
        })
    }

})


module.exports = userRouter; 