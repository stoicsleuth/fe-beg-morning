import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { Link, Navigate, Route, Routes } from 'react-router-dom'
import Home from './pages/Home'
import About from './pages/About'
import Product from './pages/Product'
import NotFound from './pages/NotFound'
import User from './pages/User'

function Router() {
  return (
    <Routes>
      <Route path="/" element={<Home />}></Route>
      <Route path="/about" element={<About />}></Route>
      <Route path="/product" element={<Product />}></Route>
      <Route path="/user/:id" element={<User />}></Route>
      {/* Whenever we land here, we should be redirected to the home page */}
      <Route path="/restricted" element={<Navigate to="/" />}></Route>
      <Route path="*" element={<NotFound />}></Route>
    </Routes>
  )
}

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <nav>
        <ul>
          {/* <a href='/about' target='_blank'>About New Tab</a> */}
          <li><Link to="/">Home</Link></li>
          <li><Link to="/about">About</Link></li>
          <li><Link to="/product">Product</Link></li>
          <li><Link to="/user/1">User 1</Link></li>
          <li><Link to="/user/2">User 2</Link></li>

        </ul>
      </nav>
      <Router />
    </>
  )
}

export default App
