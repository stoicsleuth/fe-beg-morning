import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import LazyApp from './componnents/LazyApp.jsx'
import CallbackApp from './componnents/CallbackApp.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <CallbackApp />
  </React.StrictMode>,
)
