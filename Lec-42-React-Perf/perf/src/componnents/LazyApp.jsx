import { useState, useEffect, Suspense, lazy } from 'react'
import { BrowserRouter as Router, Route, Routes, Link,  } from 'react-router-dom';

// import './App.css'
// import Home from './componnents/Home';
// import Contact from './componnents/Contact';
// import AboutUs from './componnents/AboutUs';
// import Navbar from './componnents/Navbar';

const HomePage = lazy(() => import("./Home"))
const AboutPage = lazy(() => import("./AboutUs"))
const ContactPage = lazy(() => import("./Contact"))

function LazyApp() {
  return (
    <Router>
          <nav>
        <ul>
            <li>
                <Link to="/">Home</Link>
            </li>
            <li>
                <Link to="/about">About</Link>
            </li>
            <li>
                <Link to="/contact">Contact Us</Link>
            </li>
        </ul>
    </nav>
        <Suspense fallback={<div>Loading...</div>}>
        <Routes>

            <Route path="/" element={<HomePage />} />
            <Route path="/about" element={<AboutPage />} />
            <Route path="/contact" element={<ContactPage />} />
            </Routes>
        </Suspense>
    </Router>
  )
}

export default LazyApp
