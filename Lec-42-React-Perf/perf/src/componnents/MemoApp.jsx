import React, { useState, useMemo } from 'react'

const generateLargeArray = () => {
    const largeArray = []
    for(let i = 0; i < 100000000; i ++) {
        largeArray.push(i)
    }

    return largeArray
}

const sumArray = (arr) => {
    return arr.reduce((acc, cur) => acc + cur)
}


function MemoApp() {
  const [count, setCount] = useState(0)

  const sum = useMemo(() => {
    const largeArray = generateLargeArray()
    return sumArray(largeArray)
  }, [])


  return (
    <div>MemoApp With Counter
        <p>
        {count} is the value
        </p>
        <p>{sum} is the sum of generateArray</p>
        <button onClick={() => setCount(count + 1)}>Increase Count</button>
    </div>
  )
}

export default MemoApp


// Homework
// Implement a memo function in JS
// sum()
// memo(sum) -> return a function which memoizes arguments
// const memoizedSumArray = memo(sum)
// memoizedSumArray(1, 2, 3)
// memoizedSumArray(1, 2, 3) -> This sholuld not recalculate