import React, { useState, useCallback } from 'react'

const ItemList = () => {
    const [items, setItems] = useState(['Item 1', 'Item 2', 'Item 3']);
  
    // const removeItem = (itemToRemove) => {
    //   setItems((prevItems) => prevItems.filter((item) => item !== itemToRemove));
    // };

    const removeItem = useCallback((itemToRemove) => {
        setItems((prevItems) => prevItems.filter((item) => item !== itemToRemove));
    }, [])
  
    return (
      <div>
        {items.map((item) => (
          <div key={item}>
            {item} 
            {/* <RemoveComponent removeItem={removeItem} /> */}
            <button onClick={() => removeItem(item)}>Remove</button>
          </div>
        ))}
      </div>
    );
  };
  
  export default ItemList;


//   const a = 1

//   function fn1() {
//     const b = 5

//     return function fn2() {
//         const c = 10

//         return function fn3 () {       /// store all values in parent scope
//             console.log(a + b + c)
//         }
//     }
//   }