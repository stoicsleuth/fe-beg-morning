import { useState, useEffect } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';

import './App.css'
// import Home from './componnents/Home';
// import Contact from './componnents/Contact';
// import AboutUs from './componnents/AboutUs';
import Navbar from './componnents/Navbar';

function App() {
  const [Homepage, setHomePage] = useState(null)
  const [AboutPage, setAboutPage] = useState(null)
  const [ContactUsPage, setContactUsPage] = useState(null)

  useEffect(() => {
    import("./componnents/Home").then((file) => {
      console.log({ file})
      setHomePage(file.default)
    })
      
  }, [])

  const loadHomePage = () => {
    import("./componnents/Home").then((file) => setHomePage(file.default))
  }

  const loadAboutPage = () => {
    import("./componnents/AboutUs").then((file) => setAboutPage(file.default))
  }

  const loadContactPage = () => {
    import("./componnents/Contact").then((file) => setContactUsPage(file.default))
  }

  console.log({ Homepage })

  return (
    <Router>
          <nav>
        <ul>
            <li>
                <Link onClick={loadHomePage} to="/">Home</Link>
            </li>
            <li>
                <Link onClick={loadAboutPage} to="/">About</Link>
            </li>
            <li>
                <Link onClick={loadContactPage} to="/">Contact Us</Link>
            </li>
        </ul>
    </nav>
      <Routes>
        <Route path="/" element={Homepage ? Homepage: <div>Loading</div>} />
        <Route path="/" element={ContactUsPage ? ContactUsPage :<div>Loading</div>} />
        <Route path="/" element={AboutPage ? AboutPage : <div>Loading</div>} />
      </Routes>
    </Router>
  )
}

export default App
