const person = {
    name: 'John'
}

console.log(person)
console.log(person.hasOwnProperty("toString"))
console.log(person.toString())

const testArray = [1, 2, 3, 4, 5]

// testArray.map
console.log(testArray)