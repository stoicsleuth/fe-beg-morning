function A() {}
function B() {}

let a = new A()
let b = new B()

A.prototype = b
B.prototype = a

console.log(a, b)