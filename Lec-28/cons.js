function Car(model, year){
    this.model = model;
    this.year = year;

    this.nestedObj = {
        model: this.model
    }

    // this.displayInfo = function() {
    //     console.log(`${this.model}, ${this.year}`)
    // }
}


// Never do this, this will add it to all objects/arrays/function created
// Object.prototype.displayInfo = function() {
//     console.log(`${this.model}, ${this.year}`)
// }
Car.prototype.displayInfo = function() {
        console.log(`${this.model}, ${this.year}`)
}

// Car.prototype.__proto__.randomfunc = function() {
//     console.log("From random function.")
// }

// Car.prototype.value = 100

const car1 = new Car("Toyota Camry", 2020)
const car2 = new Car("Tesla MOdel S", 2022)

console.log(car1)
console.log(car2)

console.log(car1.displayInfo())


// const person = {
//     name: 'Bruce Wayne',
//     alias: "Retired Batman"
// }

// const test = [12, 3, 4,5 ]

// console.log(test)