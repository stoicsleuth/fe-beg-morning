class Car{
    constructor(model, year) {
        this.model = model;
        this.year = year;
    }

    displayInfo() {
        console.log(`${this.model}, ${this.year}`)
    }

    // this.displayInfo = function() {
    //     console.log(`${this.model}, ${this.year}`)
    // }
}

class EV extends Car {
    constructor(model, year, type){
        super(model, year)
        this.type = type
    }
}

const ev1 = new EV("Tesla", 2021, "Type II")
console.log(ev1)

const newCar1 = new Car("Toyota camri", 2022)
console.log(newCar1)