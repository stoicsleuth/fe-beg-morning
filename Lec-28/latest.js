const newArray = [3, 4, 5]

Array.prototype.forEach = () => {
    console.log("Oh no! I am not implemented!")
}

// const obj1 = {
//     a: 10
// }
// obj1.a = 20

console.log(newArray)

newArray.forEach((p) => console.log(p))

const anotherArray = [10, 20, 20]
console.log(anotherArray)
anotherArray.forEach = () => {
    console.log("AM I implemented?")
}

anotherArray.forEach()
