// You want some common function added to the object prototypes
// But you don't want to use constructors/classes


// class TestObj {
//     logTime() {
//         console.log(Date.now())
//     }
// }

// const obj1 = new TestObj()


function logTime() {
    console.log(Date.now())
}

const obj1 = Object.create({
    logTime,
    testFn: () => {
        console.log("Performing some operation")
    }
})
obj1.value = 10
console.log(obj1)
console.log(obj1.logTime())


// 3. ways of modifying prototypes
// 1. Constructors -> Car.prototype.somefn = ()
// 2. Class -> Simply use class methods
// 3: One off instances where you want to directly modify prototype -> Object.create()