const parent = document.querySelector("#colorPalette")

const COLORS_ARRAY = [
    'red',
    'green',
    'blue',
    'orange',
    'tomato',
    'cyan',
    'purple',
    'magenta'
]

// ffffff  = [0123456789abcdef]
// "Red" -> #hsyansj
// rgb(0, 0, 0) == 'red'

parent.addEventListener('click', (e) => {
    const childrenClicked = e.target
    const restrictedColor = childrenClicked.innerText

    // Generate some new color
    // Change its color

    let newIndex = Math.floor(Math.random() * COLORS_ARRAY.length)
    let newColor = COLORS_ARRAY[newIndex]

    const currentColor = childrenClicked.style.backgroundColor

    while(newColor.toLowerCase() == restrictedColor.toLowerCase() || newColor.toLowerCase() == currentColor.toLowerCase()) {
        newIndex = Math.floor(Math.random() * COLORS_ARRAY.length)
        newColor = COLORS_ARRAY[newIndex]
    }

    childrenClicked.style.backgroundColor = newColor
})