// Initially the textbox and reply is visible
// Whenever we click on "Comment"
// Check if there is any comment
// if there is, add a comment element to the CommentsContainer
// That element should contain a couple of things
// Comment Text
// Reply Button
// Replycontainer
// Add an event listener to the "Reply" button of each comment
// Whenever I click on reply
// Check if anything is present in reply textarea
// Add a reply element in the replyContainer

// Selection
const submitButton = document.getElementById("submitComment")
const commentInput = document.getElementById("commentInput")
const commentsContainer = document.getElementById("commentsContainer")

submitButton.addEventListener('click', () => {
    // Grab comment input
    const commentText = commentInput.value.trim()

    if(commentText) {
        addComment(commentText)
        commentInput.value = ""
    }
})

function addComment(value) {
    const commentElement = document.createElement("div")
    commentElement.classList.add("comment")
    commentElement.innerHTML = `
        <p>${value}</p>
        <button class="replyBtn">Reply</button>
        <div class="repliesContainer">
        </div>
        <textarea class="replyInput" placeholder="Write a reply..."></textarea> 
    `
    commentsContainer.appendChild(commentElement)
}