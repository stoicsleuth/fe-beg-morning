const starsParent = document.getElementById('stars')
const stars = document.querySelectorAll('.star')
const ratingDisplay = document.getElementById('rating')

starsParent.addEventListener('click', (e) => {
    const star = e.target

    const value = star.getAttribute('data-value')

    updateValue(value)
})

function updateValue(value) {
    // Loop over all stars 
    // And add appropriate class i.e filled

    stars.forEach((starElem) => {
        const currentValue = starElem.getAttribute('data-value')

        starElem.classList.toggle(
            "filled",
            parseInt(currentValue, 10) <= parseInt(value, 10)
        )
    })

    ratingDisplay.innerText = value
}