// we are at a coffee shop and the now the coffee shop only has coffee we cannot order for any other drink , it will reject if any other drink is ordered and if s coffee is ordered it will be accepted then it will be processed then it will be serverd and at the end we will recieve a bill

// placeOrder -> processOrder -> serveOrder -> billOrder

function placeOrder(drink){
    return new Promise((res, rej) => {
        setTimeout(() => {
            if(drink == "coffee") {
                res("Order for coffee is received")
            } else {
                rej("Please go to the tea shop!")
            }
        }, 1000)
    })
}


function processOrder() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res("Order is processed")
        }, 500)
    })
}

function serveOrder() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res("Order is served")
        }, 500)
    })
}

function billOrder() {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res("Order is billed")
        }, 500)
    })
}

async function orderAndEnjoyCofee() {

    try {
        let res = await placeOrder("tea")
        console.log(res)

        res = await processOrder() 
        console.log(res)

        res = await serveOrder() 
        console.log(res)

        res = await billOrder() 
        console.log(res)

        console.log("Enjoy your coffee!")
    } catch (error) {
        console.log(error)
    }
}


try {
    await orderAndEnjoyCofee()

    // Some other async tasks
} catch (error) {
    console.log(error)
}


// placeOrder("coffee").then((val) => {
//     console.log(val)

//     return processOrder()
// }).then((val) => {
//     console.log(val)

//     return serveOrder()
// }).then((val) => {
//     console.log(val)

//     return billOrder()
// }).then((val) => {
//     console.log(val)

//     console.log("Enjoy your coffee!")
// })