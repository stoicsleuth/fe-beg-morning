const p1 = new Promise((res, rej) => {
    setTimeout(() => {
        res("Promise Resolved")
    }, 10000)
})

const p2 = new Promise((res, rej) => {
    setTimeout(() => {
        res("Promise Resolved")
    }, 5000)
})

const dependentFunc = () => {
    console.log("This should be called after p")
}

async function handlePromise() {

    console.log("To print or not to print?")

    const val = await p1

    // This looks like a sync code
    console.log(val)
    console.log("Am I also waiting?")
    dependentFunc()

    // p2.then((val) => console.log(val2, "I am printed here")
    // )
    
    const val2 = await p2

    console.log(val2, "I am printed here")

}

handlePromise()


// const p3 = new Promise((res, rej) => {
//     res(101)
// })

// function addDependency() {
//     p3.then(() => console.log("Abc"))
// }

