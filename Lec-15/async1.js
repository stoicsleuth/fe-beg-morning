
const p = new Promise((res, rej) => {
    res("Promise Resolved")
})

async function fetchData() {
    return p
}

const value = fetchData()

value.then((res) => console.log("Promise is resolved and done."))

console.log(value)