const p1 = new Promise((res, rej) => {
    setTimeout(() => {
        rej("Error")
    }, 1000)
})

async function handleProm() {
    try {
        const res = await p1

        console.log(res, "All good")
    } catch (error) {
        console.log(error, "Something went wrong")
    }
}

handleProm()
