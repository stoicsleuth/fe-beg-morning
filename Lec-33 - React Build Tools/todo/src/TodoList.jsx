import React from 'react'

function TodoList({ todoList }) {

	console.log({ todoList })

  return (
    <div>
			{todoList.map((todoListItem, index) => (
					<div style={{ display: 'flex', gap: 20}} key={todoListItem + index}>
						<span>{todoListItem}</span>
						{/* Implement the logic to delete this item from my todoList */}
						<span>-</span>
					</div>
				)
			)}
			{/* {todoList.map((todoListItem) => {
				return (
					<div>{todoListItem}</div>
				)
			})} */}
		</div>
  )
}

export default TodoList