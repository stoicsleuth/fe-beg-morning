import { useState } from 'react'
import InputTodo from './InputTodo'
import TodoList from './TodoList'

// {
//   id: 1,
//   value: value
// }

function Todo() {
  const [todoList, setTodoList] = useState([])

  console.log(todoList)

  return (
    // React Fragment
    <> 
        <InputTodo setTodoList={setTodoList} />
        <TodoList todoList={todoList} />
    </>
  )
}

export default Todo