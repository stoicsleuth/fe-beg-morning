import React, { useState } from 'react'

function InputTodo({ setTodoList } ) {
	const [value, setValue] = useState("")


	const handleChange = (e) => {
		setValue(e.target.value)
	}

	const addTodo = () => {
		// Add this current value to the TodoList
		setTodoList((todoListPrev) => {
				// We're creating a new array here
				// Because that is how React will understand
				// That the array has changed and it will
				// Trigger a rerender 
				return [...todoListPrev, value]
		})
		setValue("")
	}

  return (
    <div>
				{/* Why are we using an inline function?
				So that we can support passing of any prop to the handleChange fn */}
        <input onChange={(e) => handleChange(e)} value={value} type="text" />
        <button onClick={addTodo}>Add Todo</button>
    </div>
  )
}

export default InputTodo