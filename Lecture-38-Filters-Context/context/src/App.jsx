import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import GrandParent from './components/GrandParent'
import FamilyContext from './context/FamilyContext'

function App() {
  const [familyName, setFamilyName] = useState("Wayne")

  return (
    <FamilyContext.Provider value={{name: familyName, setFamilyName}}>
     <GrandParent familyName={familyName} />
    </FamilyContext.Provider>
  )
}

export default App
