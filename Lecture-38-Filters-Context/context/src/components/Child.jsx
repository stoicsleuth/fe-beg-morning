import React from 'react'
import GrandChild from './GrandChild'

function Child({ familyName }) {
  return (
    <div style={{ border: "1px solid blue"}}>Child
        <GrandChild familyName={familyName} />
    </div>
  )
}

export default Child