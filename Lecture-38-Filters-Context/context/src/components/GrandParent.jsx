import React from 'react'
import Parent from './Parent'

function GrandParent({ familyName }) {
  return (
    <div style={{ border: "1px solid black"}}>GrandParent
        <Parent familyName={familyName} />
    </div>
  )
}

export default GrandParent