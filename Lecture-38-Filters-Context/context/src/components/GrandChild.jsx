import React, { useContext } from 'react'
import FamilyContext from '../context/FamilyContext'

function GrandChild({ familyName }) {
  const { name, setFamilyName } = useContext(FamilyContext)

  const handleUpdate = () => {
    setFamilyName("Kyle")
  }

//   console.log({ familyContext})


  return (
    <>
        <div style={{ border: "1px solid green"}}>GrandChild {name}</div>
        <button onClick={handleUpdate}>Change FamilyName to Kyle</button>
    </>
  )
}

export default GrandChild