import React from 'react'
import Child from './Child'

function Parent({ familyName }) {
  return (
    <div style={{ border: "1px solid red"}}>Parent
        <Child familyName={familyName} />
    </div>
  )
}

export default Parent