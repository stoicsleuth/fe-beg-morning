const express = require("express")
const app = express()

const dotenv = require("dotenv")
const mongoose = require("mongoose")
// It reads any .env file, converts them into JS object, stores them in process.env
dotenv.config()
const { PORT, DB_URL } = process.env

mongoose.connect(DB_URL).then(() => {
    console.log("Connected to our DATABASE")
}).catch((e) => console.log(e))


const {UserModel} = require("./models/users")


// Middleware 
// This is an in-build middleware
app.use(express.json())

let USERS_ARRAY = [{
    userId: 1,
    name: "Bruce",
    address: "Gotham"
}, {
    userId: 2,
    name: "Riddler",
    address: "Gotham"
}]

const json = {
    message: "You have successfully hit the / API",
    timestamp: Date.now(),
    isRoot: true
}

// Registering the routes separately

const authenticateMiddleWare = (req, res, next) => {
    console.log("Middleware 1 is called")

    if(req.headers.password === "test") {
        // Allow the user to proceed
        next()
    } else {
        res.status(500).send("You are not authenticated!")
    }
}

const testMiddleWare = (req, res, next) => {
    //Dummy middleware
    console.log("Middleware 2 is called")
    next()
}

app.post("/add-user", authenticateMiddleWare, async (req, res) => {
    try {
        const userDetails = req.body

        const user = await UserModel.create(userDetails)

        res.status(200).json({
            status: "success",
            message: "User created successfully!",
            user
        })
    } catch (error) {
        res.status(500).json({
            status: "failure",
            message: error.message
        })
    }
})

// Authenticated routes -> only users with password can access it
app.get('/users', [testMiddleWare, authenticateMiddleWare], async (req, res) => {
    try {
        const users = await UserModel.find()

        if(users.length === 0) {
            return res.status(404).json({
                status: "failure",
                message: "No users found!"
            })
        }

        res.status(200).json({
            status: "success",
            users
        })
    } catch (error) {
        res.status(500).json({
            status: "failure",
            message: error.message
        })
    }
})

app.get('/users/:id', authenticateMiddleWare, async (req, res) => {
    try {
        const user = await UserModel.findById(req.params.id)

        if(!user) {
            return res.status(404).json({
                status: "failure",
                message: "No user found!"
            })
        }

        res.status(200).json({
            status: "success",
            user
        })
    } catch (error) {
        res.status(500).json({
            status: "failure",
            message: error.message
        })
    }
})


app.delete("/delete-user", (req, res) => {
    const formData = req.body

    USERS_ARRAY = USERS_ARRAY.filter((user) => {
        return user.userId !== formData.userId
    })

    res.json({
        success: true
    })
})


// Un-authenticated -> everyone can access it
app.get('/hello', (req, res) => {
    const json = {
        message: "You have successfully hit the /hello API",
        timestamp: Date.now()
    }

    // Automatically set tyhe content header
    res.json(json)
})

// In build middleware that runs for any route not specified above
// // Catch-all route to indicate 404 error
// app.use((req, res) => {
//     res.status(404).send("This route was not found")
// })

app.listen(PORT, () => {
    console.log("Application has started to run!")
})