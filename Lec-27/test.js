class Pizza {
    static totalPizzasMade = 0
    static secondVariable = 10

    constructor(toppings, size, crust) {
        this.toppings = toppings
        this.size = size
        this.crust = crust

        // Whenever we access static properties of classes
        // We ALWAYS need to access it lile Class.property
        Pizza.totalPizzasMade++
    }
    describe() {
        console.log(`A pizza of size ${ this.size } and crust ${ this.crust }`)
    }

    static returnTotalPizzasMade() {
        console.log(`Total no of pizzas made: ${Pizza.totalPizzasMade}`)
    }
}
class StuffedCrustPizza extends Pizza {
    constructor(toppings, size, crust, stuffingType) {
        super(toppings, size, crust);
        this.stuffingType = stuffingType;
    }

    describeStuffing() { 
        super.describe();
        console.log(`Stuffing of pizza is ${this.stuffingType}`); 
    }
} 
const obj1 = new StuffedCrustPizza(['cheese', 'olive'], 'medium', 'thin', 'chicken');
obj1.describeStuffing();
const obj2 = new StuffedCrustPizza(['cheese', 'olive'], 'medium', 'thin', 'chicken');
console.log(Pizza.returnTotalPizzasMade())