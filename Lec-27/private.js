class MyPizza {
    // Private properties
    #secretIngredient
    // static prop
    constructor(toppings, size, crust) {
        this.toppings = toppings
        this.size = size
        this.crust = crust
        this.#secretIngredient = "Pinapple"
    }

    desribe() {
        console.log(`A pizza of size ${this.size} and crust ${this.crust}`)
    }

    secret() {
        console.log(`The secret ingredient for today is ${this.#secretIngredient.length} letters!`)
    }

    // static superSecret() {
    //     console.log(Pizza.prop)
    // }
}

const order1 = new MyPizza(['cheese', 'olive'], 'medium', 'thin')
const order2 = new MyPizza(["veggies"], "small", "thin")
order1.toppings = []
