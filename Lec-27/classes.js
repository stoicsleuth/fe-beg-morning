class Myclass {
    constructor(prop1, prop2) {
        // Initialization of specific properties of the objects of the class
        this.prop1 = prop1
        this.prop2 = prop2
    }

    mymethod() {
        console.log(this.prop1, this.prop2)
        // Method implementation
    }
}

const obj1 = new Myclass(100, 200)
obj1.mymethod()

// New Object
// const ob1 = {}
// ob1.prop1 = "aaaa"