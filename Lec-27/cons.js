function Pizza(toppings, size, crust){
    this.toppings = toppings
    this.size = size
    this.crust = crust

    this.desribe = function() {
        console.log(`A pizza of size ${this.size} and crust ${this.crust}`)
    }
}

class MyPizza {
    constructor(toppings, size, crust) {
        this.toppings = toppings
        this.size = size
        this.crust = crust
    }

    desribe() {
        console.log(`A pizza of size ${this.size} and crust ${this.crust}`)
    }
}

const order1 = new MyPizza(['cheese', 'olive'], 'medium', 'thin')
const order2 = new MyPizza(["veggies"], "small", "thin")
order1.desribe()
order2.desribe()