
class MyPizza {
    constructor(toppings, size, crust) {
        this.toppings = toppings
        this.size = size
        this.crust = crust
    }

    describe() {
        console.log(`A pizza of size ${this.size} and crust ${this.crust}`)
    }
}

// function test(a, b, c) {
//     ...StuffedCrustPizza.
// }
// test(3, 2, 1)

class StuffedCrustPizza extends MyPizza {
    constructor(toppings, size, crust, stuffingType) {
        super(toppings, size, crust)
        // this.toppings = toppings
        // this.size = size
        // this.crust = crust
        // Assign the new property for this class
        this.stuffingType = stuffingType
    }

    describeStuffing() {
        console.log(`Stuffing is of type ${this.stuffingType}`)
    }

    randomMethod() {
        super.describe()
    }

    // Write a function that calls parents class's describe
    // And this class's describeStuffing
    describe() {
        // Calling super.anyMethod will invoke its parent class's method
        super.describe()
        this.describeStuffing()
        // console.log("Am I supposed to run?")
    }
}

const stuffedObj1 = new StuffedCrustPizza(["veggies"], "small", "thick", "veggies")
// Because there is a method called describe in my child fn,
// That's why it will override the parent class's describe
stuffedObj1.randomMethod()
